# Assignment: Rank Jure’s Waitlist Experiment Idea Using the ICE Framework

Practice using the Impact, Confidence, and Ease (ICE) Framework you learned in this section of the course.

In a brainstorming session that generated ideas for growth hacking the Google Merch Store, Jure proposed that we create a landing page where people can sign up to get a chance to purchase a limited edition Google t-shirt as a way of increasing acquisitions. How would you score this idea using the ICE framework?

## Questions for this assignment

Using the Impact, Confidence, and Ease framework, what values (on a scale of 1-10) would you assign to I, C, and E, for this idea and why?

I 7
C 6
E 8

Would expect a fairly high impact, lower level of confidence, and high level of ease. It might not be the most compelling offer, and less easy to gain traction in order for it to work.
