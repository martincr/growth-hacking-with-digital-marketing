# Assignment: Use Google Merch Store Data to Find Language/Market Fit Ideas

Practice using what you see in your data to generate ideas for Language / Market Fit tests launched through Facebook Dynamic Creative.

Using the data about the Richard Segment’s behavior on the Google Merch Store, brainstorm three above-ad text snippits and three headlines that you would test with Facebook Dynamic Creative Ads to build towards Language/Market Fit for the Richard Persona.

Here are two tips related to writing text for social ads:

Keep the Text Short and Clear. Remember, when people access Facebook and Instagram from their mobile device, they spend an average of 1.7 seconds with a piece of content.

Use the 10 Second Rule. Would the audience be able to tell a friend what you’ve advertised to them in less than 10 seconds?

Here's a cheat sheet borrowed from Eazl's Facebook / Instagram advertising course for you to use if you'd like to.

Questions for this assignment

- What are three above-ad text snippits that you might test with a Dynamic Creative Ad?

1. Get the latest Nest products at the Google Store.
2. Get a laptop bag to protect your computer, accessories, and more at the Google Store.
3. Bring the power of Google to your office. Buy online from the Google Merch Store.

- What are three headlines that you might test with a Dynamic Creative Ad?

1. Orders over \$25 ship free
2. Check out the latest swag in the Google Merch Store
3. The new apparel hotness only from Google
