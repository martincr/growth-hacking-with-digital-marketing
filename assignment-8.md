# Assignment: Look Into the Customer Journey of Your Instagram Fans

Build your skills using tools that enable you to visualize customer journeys.

## Assignment instructions

You’re leading a campaign to get your cohort of customers who discovered your brand on Instagram to repurchase. Head over to the Facebook Analytics Demo account (you’ll need a Facebook account to access it). When you’re inside, navigate to Activity → Journeys.

## Questions for this assignment

Let’s say this data is from your customers. On what channel did the most customers who discovered your brand on Instagram convert?

On Instagram - 42.1% (35.6M)

!["On Instagram"](Screen Shot 2020-04-16 at 11.46.00 PM.png)

## Links

[Facebook Analytics](http://go.eazl.co/fbademo)
