# Assignment: Show Off Some of Your New Skills: Search Engine Marketing (SEM) Focus

In Maja’s practicum, you saw her analyze custom date ranges, see which devices an audience uses, and review the effectiveness of paid search keywords. These skills are required for jobs like Search Engine Marketing Specialists. Let’s see you put some of your new skills into action.

To begin, get access to Google’s Merchandise Store analytics account. (Need help?)

Now, change the timeframe you’re analyzing from the default window to the past month.

- For Question 1: Navigate to Audience → Mobile → Overview
- For Question 2: Navigate to Acquisition → Campaigns → Paid Keywords

Questions for this assignment

## How much revenue was generated from users on mobile devices during the past month?

\$1,802.00

![Answer 1](Screen Shot 2020-04-16 at 4.30.05 PM.png)

## Which paid keyword or phrase is the weakest performer? Justify your answer using the data you see.

Google Backback is the weakest performer based upon the following criteria: the lowest average session duration (table sorted by this parameter), low number of pages per session, and relatively high bounce rate.

![Answer 2]((Screen Shot 2020-04-16 at 4.35.26 PM.png)
