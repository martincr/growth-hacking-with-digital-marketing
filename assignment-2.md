# Assignment: Create a New Buying Feature in the Google Merch Store for the Richard Persona

Use the analytics tools you've learned how to use (e.g. Google Analytics and Facebook Audience Insights) to create a new buying feature in Google's Merch Store that will be promoted to people in the Richard Persona group. You can use the persona PDF we built in Xtensio as a reference point, too.

Above, you see the Richard persona captured in a sheet we built using [Xtensio](https://xtensio.com/user-persona/). You'll notice that one of this persona's frustrations is the struggle with "everyday life--why choices." For example, simple decisions like what to wear seem bothersome to the Richard persona. Knowing this information, your job is to plan a new buying feature in the Google Merch store that will increase conversions for people like Richard, and also to develop a messaging strategy for your feature.

Questions for this assignment

- You know Richard’s pain points include the “everyday decisions.” How could you change something about the Google Merch Store to make it have a higher “must-have” quality for people like Richard?

Generally men's apparel appears popular with the persona. Greater amount of signposting from the homepage and store pages may increase conversions.

Google Analytics > Conversions > Ecommerce > +Add Segment > +New Segment

!["Richard persona, six month view"](Screen Shot 2020-04-16 at 5.29.53 PM.png)

Google Analytics > Conversions > Ecommerce > Product Performance

Google Analytics > Behavior > Overview
Click on Product Categories (Content Group)

Google Analytics > Behavior > Site Content > All Pages

Google Analytics > Behavior > Overview
Set date range to be broader (six months, for example)

- Based on your response in question 1, how what kind of messaging would you use to announce the new feature?

Messaging promoting the simple ordering and shipping process.

!["High performing pages"](Screen Shot 2020-04-16 at 5.33.10 PM.png)

- Go into the Google Merch Store Google Analytics account, create a segment for the Richard Persona, and navigate to the source/medium part of Google Analytics. After looking at these data, what are two acquisition channels you might use to promote this new feature to this persona?

!["Yahoo! organic traffic, CPC"](Screen Shot 2020-04-16 at 5.54.17 PM.png)

Yahoo! organic traffic and Google CPC are the two acquisition channels I would choose to promote (two longest average session durations). Two tactics to experiment with are targeted ads on the Yahoo! network, and CPC ads targeting men's apparel for the demographic and location.

## Links

- [How to Build the Richard Persona in Xtensio (Eazl Growth Hacking 2019)](https://www.youtube.com/watch?v=ljxCF29e91A)
- [Facebook for Business](https://business.facebook.com/home/accounts)
