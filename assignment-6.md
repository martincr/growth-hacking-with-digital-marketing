# Assignment: Create Your First Custom Report and Pilot an SEO Strategy

Let's build your intuition around creating custom analytics reports and use one to design an SEO strategy.

To work on this challenge:

Log into the Google Merch Store Analytics Account

Click the “Customization” Tab → + New Custom Report

Add these metrics to Metric Groups:

- Goal Conversion Rate
- Sessions
- Goal Completions
- Organic Searches

Create a title for the report and name the tab

Add Search Query to the Dimension Drilldowns section

Click “Save”

Deselect the Richard Persona and select “All Users”

Expand the results window to the last year

Sort by the number of goal completions

!["Custom report"](Screen Shot 2020-04-16 at 11.07.00 PM.png)

Questions for this assignment

Using this report, what keywords might you focus on for SEO efforts to attract more total conversions?

1. Variations on the terms "merchandise", and "store" appear frequently so I would propose experimenting with these keywords and similar.
