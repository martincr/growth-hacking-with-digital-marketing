# Assignment: Use Hotjar Recordings to Generate Landing Page Optimization Ideas

As a modern digital marketer, you can use many new tools to drive your strategies. In this Assignment, practice using Hotjar user recordings.

Hotjar is a tool that marketers use to see how users interact with digital properties. Watch the linked Hotjar user recordings, which capture user behavior on a landing page, and then propose an idea to improve the landing page based on what you’ve observed.

An important note: you’ll see a red dot every time a user attempts to click something--even if what they tried to click wasn’t a hyperlink.

Recording 1: https://insights.hotjar.com/r?site=1236909&recording=2528393333&token=c19286219d94bd76cd73809a25c6b1d9

Recording 2: https://insights.hotjar.com/r?site=1236909&recording=2510645420&token=09e543297351fe572f26c1b976e7904f

Recording 3: https://insights.hotjar.com/r?site=1236909&recording=2458654621&token=763a8ef536971e7769db13a595827f88

## Questions for this assignment

Based on the Hotjar recordings you watched, what user experience upgrades would you make to this landing page?

1. Make calls to action aligned to images, social proof statements.
2. Shorten the page length.
3. Make introductory copy more readable.
4. Make images clickable.
