# Assignment: How Would You Expand the Google Merch Store Beyond the US

Imagine that you’ve just taken over as the head of international growth for the Google Merch Store. What’s your first move?

## Assignment instructions

You're becoming a skilled growth hacker. Show off your new skills!

## Questions for this assignment

What’s the first country you would target for expansion outside the US?

Canada. Lower bounce rates, higher transactions, and higher conversion rate.

!["Highlighted values"](Screen Shot 2020-04-16 at 11.19.05 PM.png)

Name one product currently sold in the Store that you’d focus on selling to people in this country. Why did you choose this product and country?

YouTube-branded merchandise appears highest in the Canadian segment.

!["YouTube Merchandise"](Screen Shot 2020-04-16 at 11.27.58 PM.png)
